const axios = require('axios')
const cheerio = require('cheerio')
const url = require('url')

const webpage = 'https://cooking.nytimes.com/recipes/1017518-panzanella-with-mozzarella-and-herbs?title=recipe-title&content=recipe-topnote-metadata'
const webaddress = new url.URL(webpage)
const parameters = webaddress.searchParams

let printData = (html)=> {
    data = [];
    const $ = cheerio.load(html);

    for(var entry of parameters.entries()) { 
        let item = {};
        item[entry[0]] = $('.' + entry[1]).text();
        data.push(item);
     }

    console.log(data);
  }
  
axios.get(webpage)
.then((response)=> {
    printData(response.data)
})
.catch((error)=> {
    console.log(error)
})





