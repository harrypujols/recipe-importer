# recipe-importer

This is an incomplete work to meet the requirements of the challenge posted at https://join.jumboprivacy.com/javascript_instructions working and tested in nodejs v10.15.3, works only in the command line

@TODO:

- Run a loop that catches all the class instances of each url parameter.

- An algorithm that dynamically search for the content in the parameter's keys, making the manual entry of the class name unnecessary.

- Run this script on a Web server so results are visible on the browser, not the command line.

## To Install
```
npm install
```

## To Run

```
npm start
```

Or

```
node index.js
```

## Instructions
- On the file **index.js** change the url of the Web page in the variable **webpage**.
- proceed to insert parameters in the url based on the data you would like to extract. The value of the parameter must be the class of the tag where the data is located.
- Run the script as described above, and the data should be displayed in the command line.


## Libraries
#### Axios
The library *node-fetch* alone was not working well in tests, so decided to use this lightweight library.

#### Cheerio
A lightweight library that substitutes querySelectors with jQuery-style syntax, chosen to make prototyping faster and code less verbose.

## Thanks
Montpellier, August 6, 2019.
